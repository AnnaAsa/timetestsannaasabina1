package AppManager;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.BrowserType;

import java.util.concurrent.TimeUnit;

public class AppManager {
 WebDriver wd;



  private String browser;
  public SessionHelper sessionHelper;
  public ProfileHelper profileHelper;

  public AppManager(String browser) {
    this.browser = browser;
  }

  public void init(){

    if (browser == BrowserType.FIREFOX) {
      wd = new FirefoxDriver();
    }else if(browser == BrowserType.CHROME){
      System.setProperty("webdriver.chrome.driver", "src\\test\\resources\\chromedriver.exe");
      wd = new ChromeDriver();
    }else if(browser == BrowserType.IE){
      wd = new InternetExplorerDriver();
    }
    wd.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
    wd.get("http://tt-develop.quality-lab.ru/");
    sessionHelper = new SessionHelper(wd);
    profileHelper = new ProfileHelper(wd);

  }

    public void stop(){
    wd.quit();
    }


  public SessionHelper session() {
    return sessionHelper;
  }

  public ProfileHelper profile() {
    return profileHelper;
  }
}
