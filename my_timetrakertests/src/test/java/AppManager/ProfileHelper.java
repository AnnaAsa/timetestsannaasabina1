package AppManager;

import okio.Timeout;
import org.assertj.core.api.SoftAssertions;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static java.lang.Thread.sleep;
import static org.testng.AssertJUnit.assertTrue;
import static org.testng.AssertJUnit.fail;

public class ProfileHelper extends HelperBase {

  public ProfileHelper(WebDriver wd) {

    super(wd);
  }

  protected static final By Avatar = By.cssSelector(".m-topbar__userpic");
  protected static final By DropdownProfile = By.cssSelector("m-dropdown__inner");
  protected static final By DropdownProfileItem = By.cssSelector(".m-nav__link-text");
  protected static final By ProfilePage = By.id("headerName");
  protected static final By ProfileFormButton = By.cssSelector("a[href=\"/resume/263/preview\"]");
  protected static final By AvatarPhoto = By.cssSelector(".avatar");
  protected static final By ResumeContainer = By.xpath("//div[contains(@class,'tab-pane')]//div[@class='m-portlet'][1]");
  protected static final By BlocksLocator = By.cssSelector(" div.tab-pane div.m-portlet");

  // Блок резюме
  protected static final By BlockReSumeName = By.xpath("//h3[contains(text(),'Резюме')]");
  protected static final By ResumeRole = By.cssSelector(".post-name");
  protected static final By ResumeDecription = By.cssSelector(".post-description");
  protected static final By BlockReSumeButton = By.xpath("//button[contains(text(),'Редактировать резюме')]");
  protected static final By BlockResumeRoleIcon = By.cssSelector("span[data-original-title=\"Должность\"] i");
  protected static final By BlockResumeDescriptionIcon=By.cssSelector("span[data-original-title=\"Краткое резюме\"] i");
  protected static final By BlockResumeFormResume = By.xpath("//a[@class='btn btn-brand m-btn']");
  protected static final By ResumePreviewBlock =By.xpath("//div[@class='count-subsection user-info']");

  // Блок График работы
  protected static final By BlockScheduleName = By.xpath("//h3[contains(text(),'График работы')]");
  protected static final By BlockScheduleButton = By.xpath("//button[contains(text(),'Редактировать график')]");
  protected static final By ScheduleBlock = By.cssSelector(".stack-schedule");


  protected static final By ScheduleModalWindow = By.cssSelector("#popup-edit-schedule");
  protected static final By ScheduleModalWindowTitle = By.xpath("//h5[contains(text(),'Изменение графика работы')]");

  // Блок Контакты
  protected static final By BlockContantsName = By.xpath("//h3[contains(text(),'Контакты')]");
  protected static final By BlockContactButton = By.xpath("//button[contains(text(),'Редактировать контакты')]");
  protected static final By CorpotateEmail = By.cssSelector(".corporate-email");
  protected static final By AdditionalEmail = By.cssSelector(".additional-email");
  protected static final By Skype = By.cssSelector(".skype");
  protected static final By Phone = By.cssSelector(".m-list-search__result-item .phone");
  protected static final By AdditionalPhone = By.cssSelector(".additional-phone");
  protected static final By WorkDay = By.cssSelector(".m-list-search__result-item .work-day");
  protected static final By City = By.cssSelector(".m-list-search__result-item .city");

  protected static final By CorporateEmailIcon = By.cssSelector("span[data-original-title=\"E-mail корпоративный\"] i");
  protected static final By AdditionalEmailIcon = By.cssSelector("span[data-original-title=\"E-mail дополнительный\"] i");
  protected static final By SkypeIcon = By.cssSelector("span[data-original-title=\"Скайп\"] i");
  protected static final By PhoneIcon = By.cssSelector("a span[data-original-title=\"Телефон (основной)\"] i");
  protected static final By AdditionalPhoneIcon = By.cssSelector("span[data-original-title=\"Телефон (дополнительный)\"] i");
  protected static final By WorkDayIcon =By.cssSelector("a span[data-original-title=\"График\"] i ");
  protected static final By CityIcon =By.cssSelector(" a span[data-original-title=\"Адрес\"] i ");

  //Блок Окружения
  protected static final By BlockEnvironmentName = By.xpath("//h3[contains(text(),'Окружения')]");
  protected static final By BlockEnvironmentButton = By.xpath("//span[contains(text(),'Добавить устройство')]");
  protected static final By EnvironmentTable = By.cssSelector(".table-responsive");

  protected static final By BlocksName= By.xpath("//div[contains(@class, 'tab-pane active')]//h3[@class='m-portlet__head-text']");
  protected static final By BlockButtons = By.cssSelector(".btn-group");



  //модальное окно Резюме

  protected static final By ResumeModalWindow = By.xpath("//h5[@id='exampleModalLabel'][contains(text(), 'Изменение краткого резюме сотрудника')]");
  protected static final By RsumeRoleSelect= By.xpath("//select[@id ='post-id']");
  protected static final By ResumeRoleSelected = By.xpath("//select[@id ='post-id']//option[@value='1']");
  protected static final By ResumeDescriptionArea= By.xpath("//textarea[@id ='post-description']");
  protected static final By SaveResumeButton = By.xpath("//button[@class='btn btn-primary update-post-info']");
  protected static final By CloseWithoutSavingButton = By.xpath("//div[@id='popup-edit-resume']//button[@class='btn btn-secondary']");

  public void goToProfile() {
    wd.findElement(Avatar).click();
    wd.findElement(DropdownProfileItem).click();
    getNameProfile();
  }

  public String getNameProfile() {
    String profile = wd.findElement(ProfilePage).getText();

    System.out.println("Вы авторизованы под " + profile);
    return profile;
  }

  public void checkColorOfButton(){
    String buttonColor = wd.findElement(ProfileFormButton).getCssValue("background-color");
    System.out.println("Button color: " + buttonColor);
  }

  public void checkPositionOfAvatar(){
    int highestPointPhoto = wd.findElement(AvatarPhoto).getLocation().y;
    int highestPointResumeBlock = wd.findElement(ResumeContainer).getLocation().y;
    assertTrue("Photo displayed on the wrong place", highestPointPhoto < highestPointResumeBlock);

  }

  public void profilePageBlocks(List<String> blocksName){
    List<WebElement> blocks = wd.findElements(BlocksLocator);

    SoftAssertions softAssert = new SoftAssertions();

    for(int i = 0; i < blocks.size(); i++){
      String expectedName = blocksName.get(i);
      WebElement currentBlock = blocks.get(i);
      String actualName = currentBlock.findElement(BlocksName).getText();

      softAssert.assertThat(expectedName)
              .as("Имя блока не совпадает. Expected: " + expectedName + ". Actual: " + actualName)
              .isEqualTo(actualName);
    }
    softAssert.assertAll();
  }

//Проверки Блоков. Сорри, я нуб
  public void ResumeElementsPresence(String expeсtedBlockName, String expectedBlockButtonName,
                                     String expectedResumeRole, String expectedResumeDescription
                                     ){
    SoftAssertions softAssert = new SoftAssertions();

  String actualBlockName = wd.findElement(BlockReSumeName).getText();
  String actualBlockButtonName = wd.findElement(BlockReSumeButton).getText();
  String actualResumeName = wd.findElement(ResumeRole).getText();
  String actualResumeDecription = wd.findElement(ResumeDecription).getText();


  softAssert.assertThat(expeсtedBlockName).as("Имя блока не совпадает. Expected: " + expeсtedBlockName + ". Actual: " + actualBlockName).isEqualTo(actualBlockName);
  softAssert.assertThat(expectedBlockButtonName).as("Имя кнопки не совпадает. Expected: " + expectedBlockButtonName + ". Actual: " + actualBlockButtonName).isEqualTo(actualBlockButtonName);
  softAssert.assertThat(expectedResumeDescription).as("Описание должности не совпадает. Expected: " + expectedResumeDescription + ". Actual: " + actualResumeDecription).isEqualTo(actualResumeDecription);
  softAssert.assertThat(expectedResumeRole).as("Имя кнопки не совпадает. Expected: " + expectedResumeRole + ". Actual: " + actualResumeName).isEqualTo(actualResumeName);

  softAssert.assertThat(wd.findElement(BlockResumeRoleIcon).isDisplayed()).as("Иконка роли резюме отсутствует на странице").isTrue();
  softAssert.assertThat(wd.findElement(BlockResumeDescriptionIcon).isDisplayed()).as("Иконка описания резюме отсутствует на странице").isTrue();

  softAssert.assertAll();
  }


  public void SheduleElementsPresence(String expeсtedBlockName, String expectedBlockButtonName){
    SoftAssertions softAssert = new SoftAssertions();

    String actualBlockScheduleName = wd.findElement(BlockScheduleName).getText();
    String actualBlockScheduleButton = wd.findElement(BlockScheduleButton).getText();


    softAssert.assertThat(expeсtedBlockName).as("Имя блока не совпадает. Expected: " + expeсtedBlockName + ". Actual: " + actualBlockScheduleName).isEqualTo(actualBlockScheduleName);
    softAssert.assertThat(expectedBlockButtonName).as("Имя кнопки блока не совпадает. Expected: " + expectedBlockButtonName + ". Actual: " + actualBlockScheduleButton).isEqualTo(actualBlockScheduleButton);
    softAssert.assertThat(wd.findElement(ScheduleBlock).isDisplayed()).as("Блок расписанния отсутствует на странице").isTrue();
    softAssert.assertAll();
  }


  public void ContactsElementsPresence(String expeсtedContactBlockName, String expectedContactBlockButtonName, String expectedCorporateEmail,
                                       String expectedAdditionalEmail, String expectedSkype,
                                       String expectedPhone,String expectedAdditionalPhone,
                                       String expectedWorkDay, String expectedCity){
    SoftAssertions softAssert = new SoftAssertions();

    String actualBlockName = wd.findElement(BlockContantsName).getText();
    String actualBlockButtonName = wd.findElement(BlockContactButton).getText();
    String actualCorporateEmail = wd.findElement(CorpotateEmail).getText();
    String actualAdditionalEmail = wd.findElement(AdditionalEmail).getText();
    String actualSkype = wd.findElement(Skype).getText();
    String actualPhone = wd.findElement(Phone).getText();
    String actualAdditionalPhone = wd.findElement(AdditionalPhone).getText();
    String actualWorkDay = wd.findElement(WorkDay).getText();
    String actualCity = wd.findElement(City).getText();

    softAssert.assertThat(expeсtedContactBlockName).as("Имя блока не совпадает. Expected: " + expeсtedContactBlockName + ". Actual: " + actualBlockName).isEqualTo(actualBlockName);
    softAssert.assertThat(expectedContactBlockButtonName).as("Имя кнопки блока не совпадает. Expected: " + expectedContactBlockButtonName + ". Actual: " + actualBlockButtonName).isEqualTo(actualBlockButtonName);
    softAssert.assertThat(expectedCorporateEmail).as("Адрес корпоративного адреса почты не совпадает. Expected: " + expectedCorporateEmail+ ". Actual: " + actualCorporateEmail).isEqualTo(actualCorporateEmail);
    softAssert.assertThat(expectedAdditionalEmail).as("Адрес дополнительного адреса почты не совпадает. Expected: " + expectedAdditionalEmail + ". Actual: " + actualAdditionalEmail).isEqualTo(actualAdditionalEmail);
    softAssert.assertThat(expectedSkype).as("Skype не совпадает. Expected: " + expectedSkype + ". Actual: " + actualSkype).isEqualTo(actualSkype);
    softAssert.assertThat(expectedPhone).as("Телефон не совпадает. Expected: " + expectedPhone + ". Actual: " + actualPhone).isEqualTo(actualPhone);
    softAssert.assertThat(expectedPhone).as("Телефон не совпадает. Expected: " + expectedPhone + ". Actual: " + actualPhone).isEqualTo(actualPhone);
    softAssert.assertThat(expectedAdditionalPhone).as(" Дополнительный телефон не совпадает. Expected: " + expectedAdditionalPhone + ". Actual: " + actualAdditionalPhone).isEqualTo(actualAdditionalPhone);
    softAssert.assertThat(expectedWorkDay).as(" Рабочие часы не совпадают. Expected: " + expectedWorkDay + ". Actual: " + actualWorkDay).isEqualTo(actualWorkDay);
    softAssert.assertThat(expectedCity).as(" Адрес не совпадает. Expected: " + expectedCity + ". Actual: " + actualCity ).isEqualTo(actualCity);

    softAssert.assertThat(wd.findElement(CorporateEmailIcon).isDisplayed()).as("Иконка корпоративного имейла отсутствует на странице").isTrue();
    softAssert.assertThat(wd.findElement(AdditionalEmailIcon).isDisplayed()).as("Иконка дополнительного имейла отсутствует на странице").isTrue();
    softAssert.assertThat(wd.findElement(SkypeIcon).isDisplayed()).as("Иконка Скайпа отсутствует на странице").isTrue();
    softAssert.assertThat(wd.findElement(PhoneIcon).isDisplayed()).as("Иконка телефона отсутствует на странице").isTrue();
    softAssert.assertThat(wd.findElement(AdditionalPhoneIcon).isDisplayed()).as("Иконка дополнительного телефона отсутствует на странице").isTrue();
    softAssert.assertThat(wd.findElement(WorkDayIcon).isDisplayed()).as("Иконка расписания отсутствует на странице").isTrue();
    softAssert.assertThat(wd.findElement(CityIcon).isDisplayed()).as("Иконка Адреса отсутствует на странице").isTrue();

    softAssert.assertAll();
  }


  public void EnvironmentElementsPresence(String expeсtedBlockName,String expectedBlockButtonName){
    SoftAssertions softAssert = new SoftAssertions();

    String actualBlockName = wd.findElement(BlockEnvironmentName).getText();
    String actualBlockButtonName = wd.findElement(BlockEnvironmentButton).getText();

    softAssert.assertThat(expeсtedBlockName).as("Имя блока не совпадает. Expected: " + expeсtedBlockName + ". Actual: " + actualBlockName).isEqualTo(actualBlockName);
    softAssert.assertThat(expectedBlockButtonName).as("Имя кнопки блока не совпадает. Expected: " + expectedBlockButtonName + ". Actual: " + actualBlockButtonName).isEqualTo(actualBlockButtonName);
    softAssert.assertThat(wd.findElement(EnvironmentTable).isDisplayed()).as("Таблица с устройствами отсутствует на странице").isTrue();

    softAssert.assertAll();
  }

  public void openScheduleModalWindow() throws InterruptedException {

    wd.findElement(BlockScheduleButton).click();

    wd.findElement(ScheduleModalWindow).isDisplayed();
    wd.findElement(ScheduleModalWindowTitle).isDisplayed();


    System.out.println(wd.findElement(ScheduleModalWindowTitle).getText());
  }


  public void changeAndSaveResumeInfo(){

    JavascriptExecutor js = (JavascriptExecutor)wd;

    openResumeModalWindow();



    WebElement element = wd.findElement(RsumeRoleSelect);
    Select select = new Select(element);
    select.selectByValue("1");
    wd.findElement(SaveResumeButton).click();



    String actualResumeName = wd.findElement(ResumeRole).getText().replaceAll("[\\s]{2,}", " ");

    Actions action = new Actions(wd);

    action.moveToElement(wd.findElement(BlockReSumeButton))
            .click().build().perform();



    String resumeRoleFromModalWindow = wd.findElement(ResumeRoleSelected).getText().replaceAll("[\\s]{2,}", "");;

     SoftAssertions softAssert = new SoftAssertions();
     softAssert.assertThat(actualResumeName ).as("Роль не совпадает. Saved: " + actualResumeName  + ". From Modal Window: " + resumeRoleFromModalWindow ).isEqualTo(resumeRoleFromModalWindow);
     softAssert.assertAll();


    System.out.println("Должность из Профиля: "+ actualResumeName);
    System.out.println("Должность из Модального окна: "+resumeRoleFromModalWindow);


  }



  public void openResumeModalWindow(){

    wd.findElement(BlockReSumeButton).isDisplayed();
    Actions action = new Actions(wd);

    action.moveToElement(wd.findElement(BlockReSumeButton))
            .click().build().perform();

  wd.findElement(ResumeModalWindow).isDisplayed();

}

  public void changeWithoutSaveInfo()  {

    openResumeModalWindow();


    WebElement element = wd.findElement(RsumeRoleSelect);
    isElementPresent(RsumeRoleSelect);
    Select select = new Select(element);
    select.selectByValue("12");
    wd.findElement(CloseWithoutSavingButton).click();

    String actualResumeName = wd.findElement(ResumeRole).getText().replaceAll("[\\s]{2,}", " ");

    Actions action = new Actions(wd);

    action.moveToElement(wd.findElement(BlockReSumeButton))
            .click().build().perform();



    String resumeRoleFromModalWindow = wd.findElement(ResumeRoleSelected).getText().replaceAll("[\\s]{2,}", "");;

    SoftAssertions softAssert = new SoftAssertions();
    softAssert.assertThat(actualResumeName ).as("Роль не совпадает. Saved: " + actualResumeName  + ". From Modal Window: " + resumeRoleFromModalWindow ).isEqualTo(resumeRoleFromModalWindow);
    softAssert.assertAll();


    System.out.println("Должность из Профиля: "+ actualResumeName);
    System.out.println("Должность из Модального окна: "+resumeRoleFromModalWindow);

  }

  public void checkResumePreview() throws InterruptedException {
    /*String a = wd.getWindowHandle(); //сохранила в переменную
   wd.findElement(BlockResumeFormResume).click();//кликнула по резюме
    HashSet PageIds = (HashSet) wd.getWindowHandles();//создан hashset вкладок
    System.out.println(PageIds);
    wd.get("http://tt-develop.quality-lab.ru/resume/263/preview");
    wd.findElement(ResumePreviewBlock).isDisplayed();

    wd.close();*/


    String mainHandler = HelperBase.getMainWindowHandler();

    waitAndSwitchToNewWindow(getElement(BlockResumeFormResume));
    String CurrentPageUrl = getCurrentUrl();
    assertTrue("Wrong page address", CurrentPageUrl.contains("/preview"));
    wd.findElement(ResumePreviewBlock).isDisplayed();
    closeNewBrowserTab(mainHandler);
  }

}