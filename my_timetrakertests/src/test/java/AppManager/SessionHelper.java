package AppManager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SessionHelper extends HelperBase{


    public SessionHelper(WebDriver wd) {

      super(wd);
    }


  /** Login Fields*/
  protected static final By loginField  = By.id("username");
  protected static final By PasswordField  = By.id("password");
  protected static final By SubmitButton = By.id("_submit");

  /** Inside a Profile*/
  protected static final By Header =  By.cssSelector(".m-header-head");

    /** session Methods*/

    public void login(String username, String password) {
      type(loginField,username);
      type(PasswordField ,password);
      click(SubmitButton);
    }

    public void fieldsPresent(){
    wd.findElement(loginField).isDisplayed();
    wd.findElement(PasswordField).isDisplayed();
    }

  /** Profile Methods*/
  public void checkProfileOpened() throws InterruptedException {
    wd.findElement(Header).isDisplayed();
  }
}

