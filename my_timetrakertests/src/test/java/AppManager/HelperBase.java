package AppManager;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.NoSuchElementException;
import java.util.Set;
import java.util.logging.Logger;

import static java.lang.Thread.sleep;
import static org.testng.AssertJUnit.fail;

public class HelperBase {
  protected WebDriver wd;
  protected static final ThreadLocal<String> mainWindowHandler = new InheritableThreadLocal<>();


  public HelperBase(WebDriver wd) {
    this.wd =wd;
  }

  public void click(By locator) {
    wd.findElement(locator).click();
  }

  public void type(By locator, String text) {
    click(locator);
    if (text != null){
      String existingText = wd.findElement(locator).getAttribute("value");
      if (!text.equals(existingText)){
        wd.findElement(locator).clear();
        wd.findElement(locator).sendKeys(text);
      }
    }
  }


 /* public boolean isAlertPresent() {
    try {
      wd.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }*/

  protected boolean isElementPresent(By locator) {
    try{
      wd.findElement(locator);
      return true;
    } catch (NoSuchElementException ex){
      return false;
    }
  }

  public static String getMainWindowHandler() {
    return mainWindowHandler.get();
  }

  public static void setMainWindowHandler(String handler) {
    mainWindowHandler.set(handler);
  }


  public WebElement getElement(By locator) {
    return wd.findElement(locator);
  }
  void waitAndSwitchToNewWindow(WebElement clickTarget) throws InterruptedException {
    final Set<String> oldWindowSet = wd.getWindowHandles();
    sleep(500);
    clickTarget.click();
    waitForWindow(oldWindowSet);
  }

  private String waitForWindow(final Set<String> oldWindowSet) throws InterruptedException {
    String windowHandler = null;
    try {
      windowHandler = (new WebDriverWait(wd, 100000)).until(new ExpectedCondition<String>() {
        public String apply(WebDriver driver){
          Set<String> newWindowsSet = driver.getWindowHandles();
          String handler = "";
          newWindowsSet.removeAll(oldWindowSet);
          if (newWindowsSet.size() == 1) {
            handler = newWindowsSet.iterator().next();
          }
          return handler.equals("") ? null : handler;
        }
      });
    } catch (TimeoutException te){
      fail("New window hasn't opened");
    }
    wd.switchTo().window(windowHandler);
    sleep(4500); //for content loading and redirecting
    return windowHandler;
  }

  public String getCurrentUrl() {
    return wd.getCurrentUrl();
  }

  public void closeNewBrowserTab(String handler) {
    Set<String> windowHandles = wd.getWindowHandles();
    windowHandles.remove(handler);
    for (String handle : windowHandles) {
      wd.switchTo().window(handle);
      wd.close();
    }
    try {
      wd.switchTo().window(handler);
    } catch (Exception e) {

    }
  }
}