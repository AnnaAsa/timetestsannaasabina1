package tests;

import org.testng.annotations.Test;

public class ProfileTests extends TestBase {
  private java.util.List<String> List;

  @Test
  public void testGoToProfile() {
    app.session().login("Мистер АвтоМяу", "12345678");
    app.profile().goToProfile();
  }

  @Test
  public void testPositionAndColorOfProfileButton() {
    app.session().login("Мистер АвтоМяу", "12345678");
    app.profile().goToProfile();
    app.profile().checkColorOfButton();
    app.profile().checkPositionOfAvatar();
  }


  @Test
  public void testCheckBlocksOnProfilePage() {
    app.session().login("Мистер АвтоМяу", "12345678");
    app.profile().goToProfile();
    app.profile().ResumeElementsPresence("Резюме", "Редактировать резюме", "Финансист", "Test Test Te");
    app.profile().SheduleElementsPresence("График работы", "Редактировать график");
    app.profile().ContactsElementsPresence("Контакты", "Редактировать контакты",
            "yohqc@quality-lab.ru", "bokfx@quality-lab.ru (дополнительный)", "ymcdo",
            "37168116618", "88410640285 (дополнительный)", "11:00 - 15:00",
            "944526, Россия, Адыгея республика, город Адыгейск, Ленина, 79-75 (82)");
    app.profile().EnvironmentElementsPresence("Окружения", "Добавить устройство");
  }

  @Test

  public void testChangeScheduleModalWindowAppear() throws InterruptedException {
    app.session().login("Мистер АвтоМяу", "12345678");
    app.profile().goToProfile();
    app.profile().openScheduleModalWindow();
  }

  @Test
  public void testResumeInfoChanging() {
    app.session().login("Мистер АвтоМяу", "12345678");
    app.profile().goToProfile();
    app.profile().changeAndSaveResumeInfo();
  }


  @Test
  public void testResumeInfoChangingWithoutSaving() {
    app.session().login("Мистер АвтоМяу", "12345678");
    app.profile().goToProfile();
    app.profile().changeWithoutSaveInfo();
  }

  @Test
  public void testResumePreview() throws InterruptedException {
    app.session().login("Мистер АвтоМяу", "12345678");
    app.profile().goToProfile();
    app.profile().checkResumePreview();
  }
}